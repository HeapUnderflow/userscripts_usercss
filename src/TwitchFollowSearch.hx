import js.html.HtmlElement;
import js.Browser;

class TwitchFollowSearch {
    static var streamlist_selector(default, null):String = "div div div.tw-flex-wrap.tw-tower.tw-tower--300.tw-tower--gutter-sm";
    static var input_location_selector(default, null):String = "ul.tw-flex.tw-full-width.tw-tab-wrapper";
    static var card_text_selector(default, null):String = "div a.tw-interactive.tw-link.tw-link--inherit";
    static var inp_element_id(default, null):String = "uscript-tw-follow-search";

    //////// Taken from underscore.js#debounce
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    static function debounce(func:() -> Void, wait:Int, immediate:Bool = false):() -> Void {
        // Inserting the function here directly, as its copied directly from a different source, and a bit hard to
        // translate for me into "pure" haxe (at my current skill level)
        untyped __js__("
            var timeout;
            return function () {
                var context = this,
                    args = arguments;
                var later = function () {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        ");

        // This will never be reached on a JS target
        untyped __js__("// needed for haxe syntax-check");
        return func;
    }

    // Hide 
    static function hide(el:HtmlElement, h:Bool) {
        if (h) {
            el.style.display = "none";
        } else {
            el.style.display = null;
        }
    }

    static function filter(fstr:String, ?targets:Array<String>) {
        if (targets == null) {
            targets = ["title", "channel", "game"];
        }

        fstr = fstr.toLowerCase();
        var listings = Browser.document.querySelectorAll(streamlist_selector);
        var skipall = fstr == "";

        for (idx in listings) {
            var items = idx.childNodes;

            for (item in items) {
                var item = cast item;

                if (skipall) {
                    hide(item, false);
                    continue;
                }

                var lnks = item.querySelectorAll(card_text_selector);
                if (lnks.length < 2 || lnks.length > 3) {
                    if (item.classList.contains("tw-tower__placeholder")) {
                        continue;
                    }

                    trace("script outdated");
                    return;
                }

                if (targets.indexOf("title") > -1) {
                    if ((cast lnks[0]).innerText.toLowerCase().indexOf(fstr) > -1) {
                        hide(item, false);
                        continue;
                    }
                }

                if (targets.indexOf("channel") > -1) {
                    if ((cast lnks[1]).innerText.toLowerCase().indexOf(fstr) > -1) {
                        hide(item, false);
                        continue;
                    }
                }

                if (lnks.length == 3 && targets.indexOf("game") > -1) {
                    if ((cast lnks[2]).innerText.toLowerCase().indexOf(fstr) > -1) {
                        hide(item, false);
                        continue;
                    }
                }

                hide(item, true);
            }
        }
    }

    static function setup() {
        if (Browser.document.getElementById(inp_element_id) == null) {
            var bar = Browser.document.querySelector(input_location_selector);
            if (bar == null) {
                // Wait for X seconds and try again
                trace("Page not ready yet.. waiting 500ms");
                Browser.window.setTimeout(setup, 500);
                return;
            }

            var li = Browser.document.createLIElement();
            var inp = Browser.document.createInputElement();
            inp.type = "text";
            inp.placeholder = "Search...";
            inp.style.transform = "translateY(6px)";
            inp.style.backgroundColor = "#222";
            inp.style.color = "#fff";
            inp.style.border = "none";
            inp.style.padding = "2px 2px 2px 4px";
            inp.id = inp_element_id;

            var trigger = debounce(() -> {
                filter((cast Browser.document.getElementById(inp_element_id)).value);
            }, 500);

            inp.addEventListener("input", trigger);
            inp.addEventListener("change", trigger);
            inp.addEventListener("paste", trigger);
            inp.addEventListener("cut", trigger);
            inp.addEventListener("keyup", trigger);

            li.appendChild(inp);
            bar.appendChild(li);

            trace("Setup complete");
        } else {
            filter((cast Browser.document.getElementById(inp_element_id)).value);
        }
    }

    static function main() {
        trace("Twitch Follow Search starting up...");
        setup();
    }
}
