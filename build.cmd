@REM Copy UserCSS
xcopy src\*.css out\ /S /C /F /Y
@REM Copy UserScript
xcopy src\*.js out\ /S /C /F /Y
@REM Build Haxe to JS
haxe build_js.hxml