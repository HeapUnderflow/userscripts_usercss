// ==UserScript==
// @name         Twitch Follow Search
// @namespace    https://gitlab.com/HeapUnderflow/userscripts_usercss
// @version      1.0.0
// @description  Filter your followed live channels channels
// @author       HeapUnderflow
// @match        https://www.twitch.tv/directory/following/live
// @grant        none
// @run-at       document-end
// ==/UserScript==


(function () {
    "use strict";

    ////
    // TODO: Fix other "live" pages
    ////

    let select = "div div div.tw-flex-wrap.tw-tower.tw-tower--300.tw-tower--gutter-sm";
    let input_tabbar = "ul.tw-flex.tw-full-width.tw-tab-wrapper";
    let title_links = "div a.tw-interactive.tw-link.tw-link--inherit";


    //////// Taken from underscore.js#debounce
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    function setup() {
        console.log("Creating input bar");

        var tbar = document.querySelector(input_tabbar);

        if (tbar === null) { // This is needed as twitch takes **forever** to properly build its DOM
            console.log("not ready, waiting 500");
            setTimeout(setup, 500);
            return;
        }

        var cnt = document.createElement("li");
        var inp = document.createElement("input");
        inp.type = "text";
        inp.placeholder = "Search....";
        inp.style = "transform:translateY(6px);background-color:#222;color:#fff;border:none;padding: 2px 2px 2px 4px;";
        inp.id = "live-input-filter";

        var trigger = debounce(() => filter(document.getElementById("live-input-filter").value), 500);
        inp.addEventListener("input", trigger);
        inp.addEventListener("change", trigger);
        inp.addEventListener("paste", trigger);
        inp.addEventListener("cut", trigger);
        inp.addEventListener("keyup", trigger);

        cnt.appendChild(inp);
        tbar.appendChild(cnt);
    }

    function filter(fstr, targets = ["title", "channel", "game"]) {
        fstr = fstr.toLowerCase();
        var items = document.querySelector(select).children;

        for (let idx = 0; idx < items.length; idx++) {
            const item = items.item(idx);

            if (fstr === "") {
                hide(item, false);
                continue;
            }

            var links = item.querySelectorAll(title_links);
            if (links.length < 2 || links.length > 3) {
                if (item.classList.contains("tw-tower__placeholder")) {
                    continue;
                }
                console.warn("script outdated");
                return;
            }

            if (targets.indexOf("title") > -1) {
                if (links[0].innerText.toLowerCase().indexOf(fstr) > -1) {
                    hide(item, false);
                    continue;
                }
            }

            if (targets.indexOf("channel") > -1) {
                if (links[1].innerText.toLowerCase().indexOf(fstr) > -1) {
                    hide(item, false);
                    continue;
                }
            }

            if (links.length == 3 && targets.indexOf("game") > -1) {
                if (links[2].innerText.toLowerCase().indexOf(fstr) > -1) {
                    hide(item, false);
                    continue;
                }
            }

            hide(item, true);
        }
    }

    /// Hiding a item using "display"
    function hide(elm, dohide) {
        if (dohide) {
            elm.style.display = "none";
        } else {
            elm.style.display = null;
        }
    }

    // Call initial setup
    setup();
})();